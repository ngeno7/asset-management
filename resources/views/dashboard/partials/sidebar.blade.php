<!-- Sidebar user panel -->
<div class="user-panel">
  <div class="pull-left image">
    <i class="fa fa-user" style="color:orange; font-size:25px;"></i>
  </div>
</div>
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu" data-widget="tree">
  <li class="header">MAIN NAVIGATION</li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-dashboard"></i> <span>Assets</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{ route('categories.index') }}"><i class="fa fa-circle-o"></i>Categories</a></li>
      <li><a href="{{ route('assets.index') }}"><i class="fa fa-circle-o"></i>Assets</a></li>
    </ul>
  </li>
  <li class="treeview">
      <a href="#">
        <i class="fa fa-user-circle"></i> <span>Roles</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ route('roles.index') }}"><i class="fa fa-circle-o"></i>Roles</a></li>
        <li><a href="{{ route('roles.create') }}"><i class="fa fa-circle-o"></i>Add Role</a></li>
      </ul>
    </li>
    <li class="treeview">
        <a href="#">
          <i class="fa fa-user-plus"></i> <span>Users</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('users.index') }}"><i class="fa fa-circle-o"></i>Users</a></li>
          <li><a href="{{ route('users.create') }}"><i class="fa fa-circle-o"></i>Add User</a></li>
        </ul>
      </li>
</ul>
</section>
<!-- /.sidebar -->
</aside>

<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
