@extends('dashboard.app')
@section('content')
<section class="content-header">
        <h1>
          Roles
          <small>Role for different users</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="{{ route('assets.index')}}">Roles</a></li>
          <li class="active">Add Role</li>
        </ol>
        </section>
<div class="col-md-10 col-md-offset-1">
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Add Role</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form method="POST" action="{{ route('users.store') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="control-label">Name</label>
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="role_id">Role</label>
                    <select class="form-control" name="role_id" id="role_id">
                        @foreach($roles as $role)
                            <option value="{{$role->id}}">{{ $role->name }}</option>
                        @endforeach
                    </select>
                </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="control-label">E-Mail Address</label>
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            


            <div class="form-group">
                <div class="">
                    <button type="submit" class="btn btn-sm btn-primary pull-right">
                        Register
                    </button>
                </div>
            </div>
        </form>
  </div>
</div>
  @endsection