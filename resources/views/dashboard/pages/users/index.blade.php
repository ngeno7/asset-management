@extends('dashboard.app')
@section('content')

        <!-- Content Header (Page header) -->
        <section class="content-header">
             <h1>
                Assets
                <small>List of all Roles</small>
             </h1>
        <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
          <li>Roles</li>
        </ol>
        </section>
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Roles</h3>
      <a href="{{ route('roles.create') }}" class="btn btn-sm btn-primary pull-right">Add Role</a>
    </div>
    <!-- /.box-header -->
    <table class="table table-hover datatable">
        <thead>
            <th>No</th>
            <th>Name</th>
            <th>Role</th>
        </thead>
        <tbody>
            @foreach($users as $key=>$user)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ ucwords($user->name) }}</td>
                    <td>{{ ucfirst($user->role->name) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
  </div>
  @endsection