@extends('dashboard.app')
@section('content')
<section class="content-header">
        <h1>
          Asset Category
          <small>Categories for all the assets</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="{{ route('assets.index')}}">Assets</a></li>
          <li class="active">Add Asset</li>
        </ol>
        </section>
<div class="col-md-10 col-md-offset-1">
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Add Asset</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" action="{{ route('assets.store')}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
      <div class="box-body">
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" required>
        </div>
        <div class="form-group">
            <label>Category</label>
            <select class="form-control" name="category_id" id="category_id">
                @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" name="description" id="description"></textarea>
        </div>
        <div class="form-group">
                <label for="image">Image</label>
                <input type="file" accept="image/*" class="form-control" name="image" id="image">
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox" name="active" id="active" value="1"> Active
          </label>
        </div>
      </div>
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary btn-sm pull-right">Save Asset Category</button>
      </div>
    </form>
  </div>
</div>
  @endsection