@extends('dashboard.app')
@section('content')

        <!-- Content Header (Page header) -->
        <section class="content-header">
             <h1>
                Assets
                <small>List of all assets</small>
             </h1>
        <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
          <li>Assets</li>
        </ol>
        </section>
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Assets</h3>
      <a href="{{ route('assets.create') }}" class="btn btn-sm btn-primary pull-right">Add Asset</a>
    </div>
    <!-- /.box-header -->
    <table class="table table-hover datatable">
        <thead>
            <th>No</th>
            <th>Name</th>
            <th>Category</th>
            <th>Image</th>
            <th>Description</th>
            <th>Active</th>
            <th>Action</th>
        </thead>
        <tbody>
            @foreach($assets as $key => $asset)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $asset->name }}</td>
                <td>{{ $asset->category->name }}</td>
                <td>@if($asset->image)<img src="{{ $asset->image}}" style="height:50px; width:100px";> @else - @endif</td>
                <td>{{ substr($asset->description,0,25) }}</td>
                <td>@if($asset->active)<i class="fa fa-check"></i>@else <i class="fa fa-times"></i> @endif</td>
                <td>
                    <a href="{{ route('assets.edit',$asset->id)}}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                    <a href="#" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
  </div>
  @endsection