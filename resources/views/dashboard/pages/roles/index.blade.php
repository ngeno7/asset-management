@extends('dashboard.app')
@section('content')

        <!-- Content Header (Page header) -->
        <section class="content-header">
             <h1>
                Assets
                <small>List of all Roles</small>
             </h1>
        <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
          <li>Roles</li>
        </ol>
        </section>
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Roles</h3>
      <a href="{{ route('roles.create') }}" class="btn btn-sm btn-primary pull-right">Add Role</a>
    </div>
    <!-- /.box-header -->
    <table class="table table-hover datatable">
        <thead>
            <th>No</th>
            <th>Name</th>
            <th>Description</th>
            <th>Active</th>
            <th>Action</th>
        </thead>
        <tbody>
            @foreach($roles as $key=>$role)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $role->name }}</td>
                    <td>{{ substr($role->description, 0, 20) }}</td>
                    <td>@if($role->active)<i class="fa fa-check"></i>@else <i class="fa fa-times"></i> @endif</td>
                    <td>
                        <a href="{{ route('roles.edit',$role->id)}}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
  </div>
  @endsection