@extends('dashboard.app')
@section('content')

        <!-- Content Header (Page header) -->
        <section class="content-header">
             <h1>
                Assets
                <small>List of all asset categories</small>
             </h1>
        <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
          <li>Categories</li>
        </ol>
        </section>
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Asset Categories</h3>
      <a href="{{ route('categories.create') }}" class="btn btn-sm btn-primary pull-right">Add Category</a>
    </div>
    <!-- /.box-header -->
    <table class="table table-hover datatable">
        <thead>
            <th>No</th>
            <th>Name</th>
            <th>Description</th>
            <th>Active</th>
            <th>Action</th>
        </thead>
        <tbody>
            @foreach($categories as $key => $category)
            <tr>
                <td>{{ $key +1 }}</td>
                <td>{{ $category->name }}</td>
                <td>{{ substr($category->description,0,50) }}</td>
                <td>@if($category->active)<i class="fa fa-check"></i>@else <i class="fa fa-times"></i> @endif</td>
                <td>
                    <a href="{{ route('categories.edit',$category->id)}}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                    <a href="#" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
  </div>
  @endsection