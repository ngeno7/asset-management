<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('homepage');

//asset category route

Route::resource('categories', 'CategoryController');
//assets routes

Route::resource('assets', 'AssetController');
//roles routes

Route::resource('roles', 'RoleController');
//users routes

Route::resource('users', 'UserController');

