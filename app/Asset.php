<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    //
    protected $fillable = ['name', 'description', 'category_id', 'image', 'active'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
