<?php

namespace App\Http\Controllers;
use App\User;
use App\Role;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public function index() 
    {
        return view('dashboard.pages.users.index', ['users' => User::orderBy('id', 'desc')->get()]);
    }

    public function create()
    {
        return view('dashboard.pages.users.create', ['roles' => Role::orderBy('id', 'desc')->get()]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt(123456);

        User::create($data);
       
        session()->flash('flash_message', 'User added Successfully!');

        return redirect()->route('users.index');
    }

}
